

#include "UE4_LeapMotion.h"
#include "UE4_LeapMotionGameMode.h"
#include "UE4_LeapMotionPlayerController.h"

AUE4_LeapMotionGameMode::AUE4_LeapMotionGameMode(const class FPostConstructInitializeProperties& PCIP)
	: Super(PCIP)
{
	PlayerControllerClass = AUE4_LeapMotionPlayerController::StaticClass();
}


