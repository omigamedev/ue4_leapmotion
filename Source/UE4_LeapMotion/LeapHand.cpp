#include "UE4_LeapMotion.h"
#include "LeapHand.h"

ALeapHand::ALeapHand(const class FPostConstructInitializeProperties& PCIP)
	: Super(PCIP)
{
    static ConstructorHelpers::FObjectFinder<UStaticMesh> mesh(TEXT("StaticMesh'/Game/Hand.Hand'"));
    sphereComp = PCIP.CreateDefaultSubobject<USphereComponent>(this, TEXT("SphereComponent"));
    sphereComp->SetHiddenInGame(false);
    RootComponent = sphereComp;

    meshComp = PCIP.CreateDefaultSubobject<UStaticMeshComponent>(this, "StaticMeshComponent");
    meshComp->SetStaticMesh(mesh.Object);
    meshComp->SetRelativeRotation(FRotator(0, -90, 0));
    meshComp->AttachTo(RootComponent);

    SetActorTickEnabled(true);
    PrimaryActorTick.SetTickFunctionEnable(true);
    PrimaryActorTick.bCanEverTick = true;
    
    controller.addListener(listener);
}

void ALeapHand::Tick(float DeltaSeconds)
{
    if (controller.isConnected())
    {
        sphereComp->SetRelativeLocation(listener.pos);
        sphereComp->SetRelativeRotation(listener.rot);
    }
}

ALeapHand::~ALeapHand()
{
    controller.removeListener(listener);
}

void LeapListener::onInit(const Leap::Controller& controller)
{
    UE_LOG(LogClass, Log, TEXT("onInit"));
}

void LeapListener::onConnect(const Leap::Controller& controller)
{
    UE_LOG(LogClass, Log, TEXT("onConnect"));
}

void LeapListener::onFrame(const Leap::Controller& controller)
{
    //UE_LOG(LogClass, Log, TEXT("onFrame"));
    auto frame = controller.frame();
    auto hands = frame.hands();
    if (!hands.isEmpty())
    {
        const auto& h = hands[0];
        auto fingers = h.fingers();
        if (!fingers.isEmpty())
        {

        }
        
        auto hp = h.palmPosition();
        pos = FVector(hp.z, -hp.x, hp.y);

        auto hn = h.palmNormal();
        norm = FVector(hn.x, hn.y, hn.z);

        float pitch = FMath::RadiansToDegrees(h.direction().pitch());
        float yaw = FMath::RadiansToDegrees(h.direction().yaw());
        float roll = FMath::RadiansToDegrees(h.palmNormal().roll());
        rot = FRotator(-pitch, yaw, roll);
    }
}

void LeapListener::onDisconnect(const Leap::Controller& controller)
{
    UE_LOG(LogClass, Log, TEXT("onDisconnect"));
}
