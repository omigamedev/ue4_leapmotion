

using UnrealBuildTool;

public class UE4_LeapMotion : ModuleRules
{
	public UE4_LeapMotion(TargetInfo Target)
	{
		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore" });

		PrivateDependencyModuleNames.AddRange(new string[] {  });

        PublicIncludePaths.Add("C:\\LeapSDK\\include");
        PublicAdditionalLibraries.Add("C:\\LeapSDK\\lib\\x64\\Leap.lib");
        PublicDelayLoadDLLs.Add("C:\\LeapSDK\\lib\\x64\\Leap.dll");
        PublicIncludePaths.Add("C:\\LeapSDK\\lib\\x64");

		// Uncomment if you are using Slate UI
		// PrivateDependencyModuleNames.AddRange(new string[] { "Slate", "SlateCore" });
		
		// Uncomment if you are using online features
		// PrivateDependencyModuleNames.Add("OnlineSubsystem");
		// if ((Target.Platform == UnrealTargetPlatform.Win32) || (Target.Platform == UnrealTargetPlatform.Win64))
		// {
		//		if (UEBuildConfiguration.bCompileSteamOSS == true)
		//		{
		//			DynamicallyLoadedModuleNames.Add("OnlineSubsystemSteam");
		//		}
		// }
	}
}
