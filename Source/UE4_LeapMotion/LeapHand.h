#pragma once

#include "GameFramework/Actor.h"

#pragma push_macro("PI")
#undef PI
#include <Leap.h>
#pragma pop_macro("PI")

#include "LeapHand.generated.h"

class LeapListener : public Leap::Listener
{
public:
    FVector pos, norm;
    FRotator rot;

    virtual void onInit(const Leap::Controller& controller) override;
    virtual void onConnect(const Leap::Controller& controller) override;
    virtual void onDisconnect(const Leap::Controller& controller) override;
    virtual void onFrame(const Leap::Controller& controller) override;
};

/**
 * 
 */
UCLASS()
class ALeapHand : public AActor
{
    GENERATED_UCLASS_BODY()

    Leap::Controller controller;
    LeapListener listener;
    TSubobjectPtr<USphereComponent> sphereComp;
    TSubobjectPtr<UStaticMeshComponent> meshComp;

    virtual void Tick(float DeltaSeconds) override;
    virtual ~ALeapHand();
};
